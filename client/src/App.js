import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Landing from './components/Landing';
import Auth from './views/Auth'

import AuthContextProvider from './contexts/AuthContext'
import Dashboard from './views/Dashboard';
import PrivateRoute from './components/Layouts/routing/PrivateRoute';
import PostContextProvider from './contexts/PostContext';

function App() {
  return (
    <AuthContextProvider>
      <PostContextProvider>
        <Router>
          <Routes>

            <Route path='/' element={<Landing />} />

            <Route
              path='/login'
              element={<Auth authRoute='login' />}
            />

            <Route
              path='/register'
              element={<Auth authRoute='register' />}
            />

            <Route path='/dashboard' element={
              <PrivateRoute>
                <Dashboard />
              </PrivateRoute>}
            />

          </Routes>
        </Router>
      </PostContextProvider>
    </AuthContextProvider>
  );
}

export default App;
