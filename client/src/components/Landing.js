import { Navigate } from "react-router-dom";

function landing() {
    return <Navigate to='/login' replace={true} />
}

export default landing