import React, { useContext } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Button from 'react-bootstrap/Button'
import { Link } from 'react-router-dom'
import Container from 'react-bootstrap/Container'

import { AuthContext } from '../../contexts/AuthContext'

import learnItLogo from '../../assets/logo.svg'
import logoutIcon from '../../assets/logout.svg'

function NavbarMenu() {

    const {
        authState: {
            user: { username }
        },
        logoutUser,
    } = useContext(AuthContext)

    return (
        <Navbar expand='lg' bg='primary' variant='dark' className='shadow mx-auto'>
            <Container fluid>

                <Navbar.Brand className='font-weight-bolder text-white'>
                    <img src={learnItLogo} alt='learnItLogo' width='32' height='32' className='mr-2' />
                    ILearn
                </Navbar.Brand>

                <Navbar.Toggle aria-controls='right-navbar-nav center-navbar-nav' />

                <Navbar.Collapse id='center-navbar-nav' className='text-center justify-content-end' >
                    <Nav className='mr-auto text-center'>
                        <Nav.Link className='font-weight-bolder text-white' to='/dashboard' as={Link}>
                            Dashboard
                        </Nav.Link>

                        <Nav.Link className='font-weight-bolder text-white' to='/about' as={Link}>
                            About
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>


                <Navbar.Collapse id='right-navbar-nav' className='text-center justify-content-end' >
                    <Nav>
                        <Nav.Link className='font-weight-bolder text-white' disabled>
                            Welcome <strong>{username}</strong>
                        </Nav.Link>

                        <Button variant='secondary' className='font-weight-bolder text-white' onClick={logoutUser}>
                            <img src={logoutIcon} alt='logoutIcon' width='32' height='32' className='mr-2' />
                            Logout
                        </Button>
                    </Nav>
                </Navbar.Collapse>

            </Container>
        </Navbar>
    )
}

export default NavbarMenu