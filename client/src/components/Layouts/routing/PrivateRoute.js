import React, { useContext } from 'react'
import { AuthContext } from '../../../contexts/AuthContext'
import { Navigate } from 'react-router-dom'

import Spinner from 'react-bootstrap/esm/Spinner'
import NavbarMenu from '../NavbarMenu'

function PrivateRoute({ children }) {

    const { authState: { authLoading, isAuthencated } } = useContext(AuthContext)

    if (authLoading) {
        return (
            <div className='spinner-container'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    }

    if (!isAuthencated) {
        return (
            <Navigate to='/login' />
        )
    }
    return (
        <>
            <NavbarMenu />
            {children}
        </>
    )
}

export default PrivateRoute