import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import { AuthContext } from '../../contexts/AuthContext'
import AlertMassage from '../Layouts/AlertMassage';

function LoginForm() {
    const { loginUser, } = useContext(AuthContext)
    const [alert, setAlert] = useState(null)
    const [loginForm, setLoginForm] = useState({
        username: '',
        password: '',
    })
    const { username, password } = loginForm

    const onChangeLoginForm = event => {
        setLoginForm({
            ...loginForm,
            [event.target.name]: event.target.value
        })
    }

    const login = async (e) => {
        e.preventDefault()

        try {
            const loginData = await loginUser(loginForm)
            if (!loginData.success) {
                setAlert({ type: 'danger', message: loginData.message })
                setTimeout(() => setAlert(null), 5000)
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <>
            <Form className='mb-3' onSubmit={login}>
                <AlertMassage info={alert} />
                <Form.Group className='mb-3'>
                    <Form.Control
                        type='text'
                        placeholder='Username'
                        name='username'
                        onChange={onChangeLoginForm}
                        value={username}
                        autoComplete='on'
                        required
                    />
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Form.Control
                        type='password'
                        placeholder='Password'
                        name='password'
                        onChange={onChangeLoginForm}
                        value={password}
                        autoComplete='on'
                        required
                    />
                </Form.Group>

                <Button variant="success" type='submit'>Login</Button>
            </Form>
            <p>
                Don't have an account ?
                <Link to='/register'  >
                    <Button variant="info" size='sm' className='ms-3'>Register</Button>
                </Link>
            </p>
        </>
    );
}

export default LoginForm