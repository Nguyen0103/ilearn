import React, { useContext, useState } from 'react'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../contexts/AuthContext';
import AlertMassage from '../Layouts/AlertMassage';

function RegisterForm() {

    const { registerUser } = useContext(AuthContext)


    const [alert, setAlert] = useState(null)

    const [registerForm, setRegisterForm] = useState({
        username: "",
        password: "",
        confirmPassword: "",
    })

    const { username, password, confirmPassword } = registerForm

    const onChangeRegisterForm = (event) => {
        setRegisterForm({
            ...registerForm,
            [event.target.name]: event.target.value
        })
    }

    const handleOnSUbmitRegisterForm = async (e) => {
        e.preventDefault()

        if (password !== confirmPassword) {
            setAlert({ type: 'danger', message: "Passwords do not match" })
            setTimeout(() => { setAlert(null) }, 5000)
            return
        }

        try {
            const resData = await registerUser(registerForm)

            if (!resData.success) {
                setAlert({ type: 'danger', message: resData.message })
                setTimeout(() => setAlert(null), 5000)
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <>
            <Form className='mb-3' onSubmit={handleOnSUbmitRegisterForm}>
                <AlertMassage info={alert} />
                <Form.Group className='mb-3'>
                    <Form.Control
                        type='text'
                        placeholder='Username'
                        name='username'
                        value={username}
                        onChange={onChangeRegisterForm}
                        required
                    />
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Form.Control
                        type='password'
                        placeholder='Password'
                        value={password}
                        name='password'
                        onChange={onChangeRegisterForm}
                        required
                    />
                </Form.Group>

                <Form.Group className='mb-3'>
                    <Form.Control
                        type='password'
                        placeholder='Confirm Password'
                        name='confirmPassword'
                        value={confirmPassword}
                        onChange={onChangeRegisterForm}
                        required
                    />
                </Form.Group>

                <Button variant="success" type='submit'>Register</Button>
            </Form>
            <p>
                already have an account ?
                <Link to='/login'  >
                    <Button variant="info" size='sm' className='ms-3'>Login</Button>
                </Link>
            </p>
        </>
    )
}

export default RegisterForm