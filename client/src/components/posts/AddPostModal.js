import React, { useContext, useState } from 'react'

import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import { PostContext } from '../../contexts/PostContext'

const AddPostModal = () => {
    const { showAddPostModal, setShowAddPostModal, setShowToast, addPost } = useContext(PostContext)

    const [newPost, setNewPosts] = useState({
        title: '',
        description: '',
        status: 'TO LEARN',
        url: ''
    })

    const handleOnChangeField = (e) => {
        setNewPosts({
            ...newPost,
            [e.target.name]: e.target.value
        })
    }


    const handleSubmitPost = async (e) => {
        e.preventDefault()
        const resData = await addPost(newPost)
        const { success, message } = resData
        resetAddPostData()
        setShowToast({
            show: true,
            message,
            type: success ? 'success' : 'danger'
        })
    }

    const resetAddPostData = () => {
        setNewPosts({ title: '', description: '', status: 'TO LEARN', url: '' })
        setShowAddPostModal(false)
    }

    return (
        <Modal show={showAddPostModal} onHide={resetAddPostData}>
            <Modal.Header closeButton>
                <Modal.Title>What do you want to learn ?</Modal.Title>
            </Modal.Header>

            <Form onSubmit={handleSubmitPost}>
                <Modal.Body>
                    <Form.Group className='mt-3'>
                        <Form.Control
                            type='text'
                            placeholder='title'
                            name='title'
                            aria-describedby='title-help'
                            value={newPost.title}
                            onChange={handleOnChangeField}
                            required
                        />
                        <Form.Text id='title-help' muted>Required</Form.Text>
                    </Form.Group>

                    <Form.Group className='mt-3'>
                        <Form.Control
                            type='text'
                            placeholder='Youtube Url'
                            name='url'
                            value={newPost.url}
                            onChange={handleOnChangeField}
                        />
                    </Form.Group>

                    <Form.Group className='mt-3'>
                        <Form.Control
                            as='textarea'
                            row={3}
                            placeholder='Description'
                            name='description'
                            value={newPost.description}
                            onChange={handleOnChangeField}
                        />
                    </Form.Group>
                </Modal.Body>

                <Modal.Footer className='mt-3'>
                    <Button variant='danger' size='sm'>Cancle</Button>
                    <Button variant='success' type='submit' size='sm'>To Learn</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    )
}

export default AddPostModal