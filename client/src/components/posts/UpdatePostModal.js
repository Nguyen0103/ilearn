import React, { useContext, useEffect, useState } from 'react'

import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import { PostContext } from '../../contexts/PostContext'


function UpdatePostModal() {

    const {
        postState: { post },
        showUpdatePostModal,
        setShowUpdatePostModal,
        setShowToast,
        updatePost,
    } = useContext(PostContext)

    const [updatedPost, setUpdatedPost] = useState(post)

    useEffect(() => setUpdatedPost(post), [post])

    const { title, description, url, status } = updatedPost

    const handleOnChangeField = (e) => {
        return setUpdatedPost({ ...updatedPost, [e.target.name]: e.target.value })
    }

    const handleOnCloseUpdateModal = () => {
        setUpdatedPost(post)
        setShowUpdatePostModal(false)
    }

    const handleSubmitUpdateModal = async (e) => {
        e.preventDefault()
        const { success, message } = await updatePost(updatedPost)
        setShowUpdatePostModal(false)
        setShowToast({ show: true, message, type: success ? 'success' : 'danger' })
    }

    return (
        <Modal show={showUpdatePostModal} onHide={handleOnCloseUpdateModal}>
            <Modal.Header closeButton>
                <Modal.Title>Do you want Update ?</Modal.Title>
            </Modal.Header>
            <Form onSubmit={handleSubmitUpdateModal}>
                <Modal.Body>
                    <Form.Group className='mt-3'>
                        <Form.Control
                            type='text'
                            placeholder='title'
                            name='title'
                            aria-describedby='title-help'
                            value={title}
                            onChange={handleOnChangeField}
                            required
                        />
                        <Form.Text id='title-help' muted>Required</Form.Text>
                    </Form.Group>

                    <Form.Group className='mt-3'>
                        <Form.Control
                            type='text'
                            placeholder='Youtube Url'
                            name='url'
                            value={url}
                            onChange={handleOnChangeField}
                        />
                    </Form.Group>

                    <Form.Group className='mt-3'>
                        <Form.Control
                            as='select'
                            name='status'
                            value={status}
                            onChange={(handleOnChangeField)}
                        >
                            <option value='TO LEARN'>TO LEARN</option>
                            <option value='LEARNING'>LEARNING</option>
                            <option value='LEARNED'>LEARNED</option>
                        </Form.Control>
                    </Form.Group>


                    <Form.Group className='mt-3'>
                        <Form.Control
                            as='textarea'
                            row={3}
                            placeholder='Description'
                            name='description'
                            value={description}
                            onChange={handleOnChangeField}
                        />
                    </Form.Group>
                </Modal.Body>

                <Modal.Footer className='mt-3'>
                    <Button variant='danger' size='sm' onClick={handleOnCloseUpdateModal}>Cancle</Button>
                    <Button variant='success' type='submit' size='sm'>Update</Button>
                </Modal.Footer>
            </Form>
        </Modal>
    )
}

export default UpdatePostModal