import React, { createContext, useEffect, useReducer } from 'react';
import userService from '../services/userService';
import { authReducer } from '../reducers/AuthReducer';
import { LOCAL_STORAGE_TOKEN_NAME, SET_AUTH } from '../constants';
import { setAuthToken } from '../utils/setAuthToken';

export const AuthContext = createContext();

const AuthContextProvider = ({ children }) => {

    const [authState, dispatch] = useReducer(
        authReducer,
        {
            authLoading: true,
            isAuthencated: false,
            user: null
        }
    )

    //Authenticate user
    const loadUser = async () => {
        if (localStorage[LOCAL_STORAGE_TOKEN_NAME]) {
            setAuthToken(localStorage[LOCAL_STORAGE_TOKEN_NAME])
        }

        try {
            const resData = await userService.userAuth()
            if (resData.data.success) {
                dispatch({
                    type: SET_AUTH,
                    payload: { isAuthencated: true, user: resData.data.user }
                })
            }
        } catch (error) {
            localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME)
            setAuthToken(null)
            dispatch({
                type: SET_AUTH,
                payload: { isAuthencated: false, user: null }
            })
        }
    }
    useEffect(() => { loadUser() }, [])

    const loginUser = async (loginForm) => {
        try {
            const resData = await userService.userLogin(loginForm)
            const { success, accessToken } = resData.data
            if (success) {
                localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, accessToken)
            }
            await loadUser()

            return resData.data
        } catch (error) {
            if (error.resData.data) return error.resData.data
            else return { success: false, message: error.message }
        }
    }

    const registerUser = async (registerForm) => {
        try {
            const resData = await userService.userRegister(registerForm)

            const { success, accessToken } = resData.data

            if (success) {
                localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, accessToken)
            }

            await loadUser()

            return resData.data
        } catch (error) {
            if (error.resData.data) return error.resData.data
            else return { success: false, message: error.message }
        }
    }

    const logoutUser = async () => {
        localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME)
        dispatch({
            type: SET_AUTH,
            payload: { isAuthencated: false, user: null }
        })
    }

    const authContextData = { authState, loginUser, logoutUser, registerUser }

    return (
        <AuthContext.Provider value={authContextData}>
            {children}
        </AuthContext.Provider>
    )
}

export default AuthContextProvider;