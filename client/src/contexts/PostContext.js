import React, { createContext, useReducer, useState } from 'react'
import { postReducer } from '../reducers/PostReducer';
import postService from '../services/postService';
import { ADD_POST, DELETE_POST, FIND_POST, POSTS_LOADED_FAIL, POSTS_LOADED_SUCCESS, UPDATE_POST } from '../constants';

export const PostContext = createContext();

const PostContextProvider = ({ children }) => {

    const [postState, dispatch] = useReducer(postReducer, {
        post: null,
        posts: [],
        postLoading: true
    })

    const [showAddPostModal, setShowAddPostModal] = useState(false)
    const [showUpdatePostModal, setShowUpdatePostModal] = useState(false)

    const [showToast, setShowToast] = useState({
        show: false,
        message: '',
        type: null,
    })

    const getPosts = async () => {
        try {
            const resData = await postService.getAllPosts()
            if (resData.data.success) {
                dispatch({ type: POSTS_LOADED_SUCCESS, payload: resData.data.posts })
            }
        } catch (error) {
            dispatch({ type: POSTS_LOADED_FAIL })
        }
    }


    const addPost = async (newPost) => {
        try {
            const resData = await postService.addNewPost(newPost)
            if (resData.data.success) {
                dispatch({ type: ADD_POST, payload: resData.data.post })
            }
            return resData.data
        } catch (error) {
            return error.resData.data
                ? error.resData.data
                : { success: false, message: 'Server error' }
        }
    }

    const deletePost = async (postId) => {
        try {
            const resData = await postService.deletePost(postId)
            if (resData.data.success) {
                dispatch({ type: DELETE_POST, payload: resData.postId })
            }
        } catch (error) {
            return error.resData.data
                ? error.resData.data
                : { success: false, message: 'Server error' }
        }
    }

    // Find post when user is updating post
    const findPost = postId => {
        const post = postState.posts.find(post => post._id === postId)
        dispatch({ type: FIND_POST, payload: post })
    }

    const updatePost = async post => {
        try {
            const resData = await postService.updatePost(post._id, post)
            if (resData.data.success) {
                dispatch({ type: UPDATE_POST, payload: resData.data.post })
            }
            return resData.data
        } catch (error) {
            return error.resData.data
                ? error.resData.data
                : { success: false, message: 'Server error' }
        }
    }


    const postContextData = {
        postState,
        getPosts,
        addPost,
        showAddPostModal,
        setShowAddPostModal,
        showUpdatePostModal,
        setShowUpdatePostModal,
        showToast,
        setShowToast,
        deletePost,
        findPost,
        updatePost
    }

    return (
        <PostContext.Provider value={postContextData}>
            {children}
        </PostContext.Provider>
    )
}

export default PostContextProvider