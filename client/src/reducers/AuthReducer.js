import { SET_AUTH } from '../constants';

export const authReducer = (state, action) => {
    const { type, payload: { isAuthencated, user } } = action

    switch (type) {
        case SET_AUTH: {
            return {
                ...state,
                authLoading: false,
                isAuthencated,
                user,
            }
        }

        default:
            return state
    }
}