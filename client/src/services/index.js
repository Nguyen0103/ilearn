import axios from 'axios';

export let https = axios.create({
    baseURL: "https://ilearn-ibtb.onrender.com/api",
    headers: {
        "Content-Type": 'application/json'
    }
})


// Add a request interceptor
https.interceptors.request.use((config) => {
    // Do something before request is sent

    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});



// Add a response interceptor
https.interceptors.response.use((response) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
});