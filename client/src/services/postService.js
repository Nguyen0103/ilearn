import { https } from '../services'


const postService = {
    getAllPosts: () => https.get('/posts'),
    addNewPost: (newPost) => https.post('/posts', newPost),
    deletePost: (id) => https.delete(`/posts/${id}`),
    updatePost: (id, updatedPost) => https.put(`/posts/${id}`, updatedPost),

}

export default postService