import { https } from '../services'

const userService = {
    userAuth: () => https.get('/auth'),
    userLogin: (loginData) => https.post('/auth/login', loginData),
    userRegister: (registerData) => https.post('/auth/register', registerData),
}

export default userService