import { https } from "../services";

export let setAuthToken = (token) => {
    if (token) {
        https.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    } else {
        delete https.defaults.headers.common["Authorization"]
    }
}