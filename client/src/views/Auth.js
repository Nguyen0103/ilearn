import React, { useContext } from 'react'

import RegisterForm from '../components/auth/RegisterForm';
import LoginForm from '../components/auth/LoginForm';

import { AuthContext } from '../contexts/AuthContext'
import { Navigate } from 'react-router-dom'

import Spinner from 'react-bootstrap/Spinner'

function Auth({ authRoute }) {

    let body;

    const { authState } = useContext(AuthContext)
    const { authLoading, isAuthencated } = authState

    if (authLoading) {
        body = (
            <div className='d-flex  justify-content-center mt-2'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    } else if (isAuthencated) {
        return <Navigate to="/dashboard" />
    } else {
        body = (
            <>
                {authRoute === 'login' && <LoginForm />}
                {authRoute === 'register' && <RegisterForm />}
            </>
        )
    }

    return (
        <div className='landing'>
            <div className="dark-overlay">
                <div className="landing-inner">
                    <h1>I Learn</h1>
                    <h4>Keep track of you are learning</h4>
                    {body}
                </div>
            </div>
        </div>
    )
}

export default Auth;