import React, { useContext, useEffect } from 'react'
import { AuthContext } from '../contexts/AuthContext'

import Button from 'react-bootstrap/Button'
import Spinner from 'react-bootstrap/Spinner';
import Tooltip from 'react-bootstrap/Tooltip';
import Toast from 'react-bootstrap/Toast'
import Card from 'react-bootstrap/Card'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'

import { PostContext } from '../contexts/PostContext';
import SinglePost from '../components/posts/SinglePost';
import AddPostModal from '../components/posts/AddPostModal';

import addIcon from '../assets/plus-circle-fill.svg'
import UpdatePostModal from '../components/posts/UpdatePostModal';

function Dashboard() {
    const { authState: { user } } = useContext(AuthContext)

    const {
        postState: { post, posts, postsLoading },
        getPosts,
        setShowAddPostModal,
        showToast: { show, message, type },
        setShowToast,
    } = useContext(PostContext)

    const handleOncloseModal = () => {
        setShowToast({
            show: false,
            message: '',
            type: null
        })
    }

    useEffect(() => { getPosts() }, [getPosts])

    let body = null
    if (postsLoading) {
        body = (
            <div className='spinner-container'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    } else if (posts.length === 0) {
        body = (
            <Card className='text-center mx-5 my-5'>
                <Card.Header as='h1'>Hi {user.username}</Card.Header>
                <Card.Body>
                    <Card.Title>Welcome to LearnIt</Card.Title>
                    <Card.Text>
                        Click the button below to track your first skill to learn
                    </Card.Text>
                    <Button
                        variant='primary'
                        onClick={() => { setShowAddPostModal(true) }}
                    >
                        LearnIt!
                    </Button>
                </Card.Body>
            </Card>
        )
    } else {
        body = (
            <>
                <Row className='row-cols-1 row-cols-md-3 g-4 mx-auto mt-3'>
                    {posts.map(post => (
                        <Col key={post._id} className='my-2'>
                            <SinglePost post={post} />
                        </Col>
                    ))}
                </Row>


                <OverlayTrigger placement='left' delay={{ show: 250, hide: 400 }} overlay={<Tooltip style={{}}>Add a post</Tooltip>}>
                    <Button className='btn-floating' onClick={() => { setShowAddPostModal(true) }}>
                        <img src={addIcon} alt='add-post' width='60' height='60' />
                    </Button>
                </OverlayTrigger>
            </>
        )
    }

    return (
        <>
            {body}
            <AddPostModal />
            {post !== null && <UpdatePostModal />}
            <Toast
                show={show}
                animation
                style={{ position: 'fixed', top: '20%', right: '10px' }}
                className={`bg-${type} text-white`}
                onClose={handleOncloseModal}
                delay={1500}
                autohide
            >
                <Toast.Body>
                    <strong>{message}</strong>
                </Toast.Body>
            </Toast>
        </>
    )
}

export default Dashboard