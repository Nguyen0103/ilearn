const mongoose = require("mongoose");
require('dotenv').config();

const connectDB = async () => {
    try {
        mongoose.connect(`mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@ilearn.qyttj8j.mongodb.net/?retryWrites=true&w=majority`)
        console.log("mongooseDB connected");
    } catch (error) {
        console.log(error)
        process.exit(1)
    }
}

module.exports = { connectDB };
