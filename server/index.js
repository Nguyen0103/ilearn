require('dotenv').config()
const express = require("express");
const cors = require('cors')

// import Router
const authRouter = require('./routes/auth')
const postRouter = require('./routes/post')

// Connect DB
const database = require('./config/db');
database.connectDB();

// callback Express
const app = express();
// parser json
app.use(express.json())
app.use(cors())

// router
app.use('/api/auth', authRouter);
app.use('/api/posts', postRouter);

const port = process.env.PORT || 5000;
// app listen
app.listen(port, () => {
    console.log(`application listing on port: ${port}`)
})