require('dotenv').config();
const jwt = require('jsonwebtoken')

const verifyToken = (req, res, next) => {
    // Will get from headers and will have Bearer in accessToken (Bearer AccessToken), we need to remove it(Bearer)
    const authHeader = req.header('Authorization')

    // remove white Space and collect 1 element in array
    const token = authHeader && authHeader.split(' ')[1]


    if (!token && res.status(401)) {
        res.json({ success: false, message: "AccessToken not found" })
    }

    try {
        const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)

        req.userId = decoded.userId

        next()
    } catch (error) {
        res.status(403).json(
            {
                success: false,
                message: "Invalid Token"
            }
        )
    }
}

module.exports = verifyToken