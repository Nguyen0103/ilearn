const mongoose = require('mongoose');
const { Schema } = mongoose;

const PostSchema = new Schema({
    title: { type: String, require: true },
    description: { type: String, require: true },
    url: { type: String },
    status: { type: String, enum: ['TO LEARN', 'LEARNING', 'LEARNED'] },
    user: { type: Schema.Types.ObjectId, ref: 'users' }
}, {
    timestamps: true
})

// 'name', Shema
module.exports = mongoose.model('posts', PostSchema)