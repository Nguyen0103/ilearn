const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema({
    username: { type: String, require: true, unique: true },
    password: { type: String, require: true },

}, {
    timestamps: true
})

// 'name', Shema
module.exports = mongoose.model('users', UserSchema)