const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');
require('dotenv').config

//middleware
const verifyToken = require('../middleware/auth')
//models
const User = require('../models/User');

/**
 *  @router GET api/auth/
 *  @desc check this user is login
 *  @access Public
 */
router.get('/', verifyToken, async (req, res) => {
    try {
        const user = await User.findById(req.userId).select('-password')
        if (!user) {
            return res.status(400).json({ success: false, message: "user not found" })
        }
        res.json({ success: true, user })
    } catch (error) {
        console.log(error)
        res.status(500).json({ success: false, message: "Internal server error" })
    }
})

/**
 * @router POST api/auth/register
 * @desc register user
 * @access Public
 */
router.post('/register', async (req, res) => {
    const { username, password } = req.body;

    // simple validation
    if (!username || !password) {
        return res.status(400).json({ success: false, message: 'Missing username and/or password' });
    }

    try {
        // check for exitsing user
        const user = await User.findOne({ username });

        if (user) {
            return res.status(400).json({ sucsess: false, message: "Username already taken" });
        }

        // All Good
        const hashedPassword = await argon2.hash(password)
        const newUser = new User({ username, password: hashedPassword })
        await newUser.save();

        // Return Token
        const accessToken = jwt.sign({ userId: newUser._id }, process.env.ACCESS_TOKEN_SECRET)

        // return result
        res.json({
            success: true,
            message: "User register successfully!!!",
            accessToken
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({ success: false, message: "Internal server error" })
    }
})

/**
 * @router POST api/auth/login
 * @desc login user
 * @access Public
 */
router.post('/login', async (req, res) => {
    const { username, password } = req.body;
    if (!username || !password) {
        return res.status(400).json({ success: false, message: "Missing username or password" })
    }
    try {
        const user = await User.findOne({ username })
        if (!user) {
            return res.status(400).json({ success: false, message: "Incorect username or password" })
        }
        const passwordValid = await argon2.verify(user.password, password)
        if (!passwordValid) {
            return res.status(400).json({ success: false, message: "Incorect username or password" })
        }
        const accessToken = jwt.sign({
            userId: user._id
        }, process.env.ACCESS_TOKEN_SECRET)

        res.json({
            success: true,
            message: "login successfully!!!",
            accessToken
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({ success: false, message: "Internal server error" })
    }
})

module.exports = router;