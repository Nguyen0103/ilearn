const express = require('express');
const router = express.Router()

const Post = require('../models/Post');
const verifyToken = require('../middleware/auth');

/**
 * @router POST api/posts/
 * @desc Create post
 * @access Private
 */
router.post('/', verifyToken, async (req, res) => {
    const { title, description, url, status } = req.body;

    if (!title) return res.status(400).json({ success: false, message: "title is required or title can't the same" })

    try {
        const newPost = new Post({
            title,
            description,
            url: (url.startsWith("https://")) ? url : `https://${url}`,
            status: status || "TO LEARN",
            user: req.userId
        })

        await newPost.save()

        res.json({ success: true, message: "Enjoy Learning", post: newPost })
    } catch (error) {
        console.log(error)
        res.status(500).json({ success: false, message: "Internal server Error" })
    }
})

/**
 * @router GET api/posts/
 * @desc GET post
 * @access private
 */
router.get('/', verifyToken, async (req, res) => {
    try {
        const posts = await Post.find({ user: req.userId }).populate('user', [
            'username'
        ])
        res.json({ success: true, posts })
    } catch (error) {
        console.log(error)
        res.status(500).json({ success: false, message: "Internal server Error" })
    }
})

/**
 * @router PUT api/posts/:id
 * @desc update posts
 * @access Private
 */
router.put('/:id', verifyToken, async (req, res) => {
    const { title, description, url, status } = req.body;
    if (!title) return res.status(400).json({ success: false, message: "title is required" })
    try {
        let updatedPost = {
            title,
            description: description || '',
            url: ((url.startsWith("https://")) ? url : `https://${url}`) || '',
            status: status || "TO LEARN",
        }

        // condition when updatedPost needed have _id and user
        const postUpdateCondition = { _id: req.params.id, user: req.userId }

        //findOneAndUpdate(condition<object>,update<object>,options<object>)
        updatedPost = await Post.findOneAndUpdate(
            postUpdateCondition,
            updatedPost,
            { new: true }
        )

        // user not authorised to update post or post not found
        if (!updatedPost) {
            return res.status(401).json({
                success: false,
                message: "Post not found or user not authorised"
            })
        }

        res.json({
            success: true,
            message: "The post is updated",
            post: updatedPost
        })

    } catch (error) {
        console.log(error)
        res.status(500).json({ success: false, message: "Internal server Error" })
    }
})

/**
 * @router DELETE api/posts/:id
 * @desc delete posts
 * @access private
 */
router.delete('/:id', verifyToken, async (req, res) => {
    try {
        const deleteCondition = { _id: req.params.id, user: req.userId }
        const deletePost = await Post.findOneAndDelete(deleteCondition)

        // user not authorised to update post or post not found
        if (!deletePost) {
            return res.status(401).json({
                success: false,
                message: "Post not found or user not authorised"
            })
        }

        res.json({
            success: true,
            message: "this post have been deleted",
            post: deletePost
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({ success: false, message: "Internal server Error" })
    }
})

module.exports = router;